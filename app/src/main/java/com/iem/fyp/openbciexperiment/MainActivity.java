package com.iem.fyp.openbciexperiment;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.choosemuse.libmuse.ConnectionState;
import com.choosemuse.libmuse.Eeg;
import com.choosemuse.libmuse.LibmuseVersion;
import com.choosemuse.libmuse.Muse;
import com.choosemuse.libmuse.MuseArtifactPacket;
import com.choosemuse.libmuse.MuseConnectionPacket;
import com.choosemuse.libmuse.MuseDataListener;
import com.choosemuse.libmuse.MuseDataPacket;
import com.choosemuse.libmuse.MuseDataPacketType;
import com.choosemuse.libmuse.MuseFileWriter;
import com.choosemuse.libmuse.MuseListener;
import com.choosemuse.libmuse.MuseManagerAndroid;
import com.choosemuse.libmuse.MuseConnectionListener;
import com.choosemuse.libmuse.MuseVersion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

import static java.lang.Math.toIntExact;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Muse muse;
    private static final int MIN_RANGE = 0;
    private static final int MAX_RANGE = 9;
    private ArrayAdapter<String> spinnerAdapter;
    private MuseManagerAndroid manager;
    private final String TAG = "TestLibMuseAndroid";
    Button button;
    private final double[] eegBuffer = new double[7];
    private boolean eegStale;
    private ConnectionListener connectionListener;
    private DataListener dataListener;
    int ids[] = {R.id.buttonA, R.id.buttonB, R.id.buttonC, R.id.buttonD, R.id.buttonE, R.id.buttonF, R.id.buttonG, R.id.buttonH, R.id.buttonI};
    ArrayList<Integer> numsGenerated;
    ArrayList<Integer> textPosGenerated;
    private int numsToGen;
    private int step;//0=not started, 1=nums initialized, 2=
    private int posSelected;
    private final Handler handler = new Handler();
    private final AtomicReference<MuseFileWriter> fileWriter = new AtomicReference<>();
    private final AtomicReference<Handler> fileHandler = new AtomicReference<>();
    private int marker=0;
    private String dataFile;
    private String fileName;//fileName format Experiment + Experiment Number + Numbers Generated + User Right/Wrong + Button Right/Wrong + User Predicted Right/Wrong
    private String folderName;
    private int expCount=0;
    private BluetoothAdapter btAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = MuseManagerAndroid.getInstance();
        manager.setContext(this);
        fileName="Experiment";

        Log.i(TAG, "LibMuse version=" + LibmuseVersion.instance().getString());

        WeakReference<MainActivity> weakActivity =
                new WeakReference<>(this);
        // Register a listener to receive connection state changes.
        connectionListener = new ConnectionListener(weakActivity);
        // Register a listener to receive data from a Muse.
        dataListener = new DataListener(weakActivity);
        // Register a listener to receive notifications of what Muse headbands
        // we can connect to.
        manager.setMuseListener(new MuseL(weakActivity));
        numsToGen = 2;
        step = 0;
        ensurePermissions();
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();
        setContentView(R.layout.activity_main);
        initUI();
        button = findViewById(R.id.button);
        Button a = findViewById(ids[0]);
        Button b = findViewById(ids[1]);
        Button c = findViewById(ids[2]);
        Button d = findViewById(ids[3]);
        Button e = findViewById(ids[4]);
        Button f = findViewById(ids[5]);
        Button g = findViewById(ids[6]);
        Button h = findViewById(ids[7]);
        Button i = findViewById(ids[8]);
        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);
        e.setOnClickListener(this);
        f.setOnClickListener(this);
        g.setOnClickListener(this);
        h.setOnClickListener(this);
        i.setOnClickListener(this);
        button.setOnClickListener(this);

        button.setText(R.string.start);
        a.setText("");
        b.setText("");
        c.setText("");
        d.setText("");
        e.setText("");
        f.setText("");
        g.setText("");
        h.setText("");
        i.setText("");
    }

@Override
    public void onClick(View view) {
    int id = view.getId();
    if (id == R.id.button) {
        dataFile="";
        if (expCount % 4 == 0 && numsToGen == 2) {
            expCount=0;
            nameAlertDialog();
        } else {
            initStep1();
        }
    }
        else if (id == R.id.refresh) {
            // The user has pressed the "Refresh" button.
            // Start listening for nearby or paired Muse headbands. We call stopListening
            // first to make sure startListening will clear the list of headbands and start fresh.
            manager.stopListening();
            manager.startListening();

        }
        else if (id == R.id.connect) {

            // The user has pressed the "Connect" button to connect to
            // the headband in the spinner.

            // Listening is an expensive operation, so now that we know
            // which headband the user wants to connect to we can stop
            // listening for other headbands.
            manager.stopListening();

            List<Muse> availableMuses = manager.getMuses();
            Spinner musesSpinner = findViewById(R.id.muses_spinner);

            // Check that we actually have something to connect to.
            if (availableMuses.size() < 1 || musesSpinner.getAdapter().getCount() < 1) {
                Log.w(TAG, "There is nothing to connect to");
            } else {

                // Cache the Muse that the user has selected.
                muse = availableMuses.get(musesSpinner.getSelectedItemPosition());
                // Unregister all prior listeners and register our data listener to
                // receive the MuseDataPacketTypes we are interested in.  If you do
                // not register a listener for a particular data type, you will not
                // receive data packets of that type.
                muse.unregisterAllListeners();
                muse.registerConnectionListener(connectionListener);
                muse.registerDataListener(dataListener, MuseDataPacketType.EEG);
                // Initiate a connection to the headband and stream the data asynchronously.
                muse.runAsynchronously();
            }

        } else if (id == R.id.disconnect) {

            // The user has pressed the "Disconnect" button.
            // Disconnect from the selected Muse.
            if (muse != null) {
                muse.disconnect();
            }
        }
        else if (step == 1) {
            int correctButtonID = textPosGenerated.get(posSelected);
            if (id == ids[correctButtonID]) {
                fileName = fileName + "C";
                new CountDownTimer(200, 200) {

                    public void onTick(long time) {
                        marker = 2;
                    }

                    public void onFinish() {
                        new CountDownTimer(400, 400) {

                            public void onTick(long time) {
                                button.setEnabled(false);
                            }

                            public void onFinish() {
                                if(expCount%4<2 && genRandom()<=20) {
                                    button.setText(R.string.wrong);
                                    button.setBackgroundColor(Color.RED);
                                    fileName=fileName+"W";
                                }
                                else{
                                    button.setText(R.string.correct);
                                    button.setBackgroundColor(Color.GREEN);
                                    fileName=fileName+"C";
                                }
                                numsToGen++;
                                if(numsToGen>9) {
                                    expCount=expCount+1;
                                    numsToGen = 2;
                                }
                                clearAfterStep1();
                                new CountDownTimer(350, 350) {
                                    public void onTick(long time) {
                                    }

                                    public void onFinish() {
                                        button.setEnabled(true);
                                        eegStale = false;
                                        marker = 0;
                                        DialogInterface.OnClickListener yesListener =
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        fileName = fileName + "C";
                                                        writeToFile(dataFile);

                                                    }
                                                };
                                        DialogInterface.OnClickListener noListener =
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        fileName = fileName + "W";
                                                        writeToFile(dataFile);
                                                    }
                                                };
                                        AlertDialog introDialog = new AlertDialog.Builder(MainActivity.this)
                                                .setTitle("Question")
                                                .setMessage("Did you expect the answer right?")
                                                .setPositiveButton("Yes", yesListener)
                                                .setNegativeButton("No", noListener)
                                                .setCancelable(false)
                                                .create();
                                        introDialog.show();
                                    }
                                }.start();
                            }
                        }.start();

                    }
                }.start();


            } else {
                fileName = fileName + "W";
                new CountDownTimer(200, 200) {

                    public void onTick(long time) {
                        marker = 2;
                    }

                    public void onFinish() {

                        new CountDownTimer(400, 400) {

                            public void onTick(long time) {
                                button.setEnabled(false);
                            }

                            public void onFinish() {
                                if(expCount%4<2 && genRandom()<=20) {
                                    button.setText(R.string.right);
                                    button.setBackgroundColor(Color.GREEN);
                                    fileName=fileName+"W";
                                }
                                else{
                                    button.setText(R.string.wrong);
                                    button.setBackgroundColor(Color.RED);
                                    fileName=fileName+"C";
                                }
                                numsToGen++;
                                if(numsToGen>9) {
                                    expCount=expCount+1;
                                    numsToGen = 2;
                                }
                                clearAfterStep1();
                                new CountDownTimer(350, 350) {
                                    public void onTick(long time) {
                                    }

                                    public void onFinish() {
                                        button.setEnabled(true);
                                        eegStale = false;
                                        marker = 0;
                                        DialogInterface.OnClickListener yesListener =
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        fileName = fileName + "C";
                                                        writeToFile(dataFile);

                                                    }
                                                };
                                        DialogInterface.OnClickListener noListener =
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                        fileName = fileName + "W";
                                                        writeToFile(dataFile);
                                                    }
                                                };
                                        AlertDialog introDialog = new AlertDialog.Builder(MainActivity.this)
                                                .setTitle("Question")
                                                .setMessage("Did you expect the answer right?")
                                                .setPositiveButton("Yes", yesListener)
                                                .setNegativeButton("No", noListener)
                                                .setCancelable(false)
                                                .create();
                                        introDialog.show();
                                    }
                                }.start();
                            }
                        }.start();
                    }
                }.start();

            }
        }
    }
    private final Runnable tickUi = new Runnable() {
        @Override
        public void run() {
            if (eegStale) {
                updateEeg();
            }
            handler.postDelayed(tickUi, 1000/60);
        }
    };

    private void updateEeg() {
        TextView tp9 = findViewById(R.id.eeg_tp9);
        TextView fp1 = findViewById(R.id.eeg_af7);
        TextView fp2 = findViewById(R.id.eeg_af8);
        TextView tp10 = findViewById(R.id.eeg_tp10);
        TextView markerStatus=findViewById(R.id.marker_status);
        tp9.setText(String.format(Locale.US, "%6.2f", eegBuffer[0]));
        fp1.setText(String.format(Locale.US, "%6.2f", eegBuffer[1]));
        fp2.setText(String.format(Locale.US, "%6.2f", eegBuffer[2]));
        tp10.setText(String.format(Locale.US, "%6.2f", eegBuffer[3]));
        markerStatus.setText(marker+"");
    }

    private void nameAlertDialog(){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                folderName=String.valueOf(userInput.getText());
                                initStep1();

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    long millis;
    private void initStep1() {

        fileName="Experiment"+expCount+numsToGen;
        eegStale=true;
        handler.post(tickUi);
        button.setText("");
        button.setTextColor(Color.BLACK);
        button.setBackgroundResource(android.R.drawable.btn_default);
        step = 0;
        posSelected = -1;
        numsGenerated = new ArrayList<>();
        textPosGenerated = new ArrayList<>();
        Log.e("num", numsToGen + "");
        final int totalTime = 1100 * (numsToGen);
        new CountDownTimer(totalTime, 1100) {
            public void onTick(long millisUntilFinished) {
                millis=millisUntilFinished;
                button.setEnabled(false);
                new CountDownTimer(100,100){
                    public void onTick(long millisUntilFinished) {
                        marker=1;
                    }
                    public void onFinish(){
                        new CountDownTimer(400,400){
                            public void onTick(long millisUntilFinished) {
                                int num, textPos;
                                while(true){
                                    num = getNum();
                                    textPos = getTextPosition();
                                    if (!(numsGenerated.contains(num) || textPosGenerated.contains(textPos))) {
                                        break;
                                    }
                                }
                                numsGenerated.add(num);
                                textPosGenerated.add(textPos);
                                Log.e("Nums Generated Size",numsGenerated.size()+"");
                                Log.e("TextPos Generated Size",textPosGenerated.size()+"");
                                int i = toIntExact(totalTime - millis)/1000;
                                int id = textPosGenerated.get(i);
                                Button b = findViewById(ids[id]);
                                b.setText(numsGenerated.get(i).toString());
                            }
                            public void onFinish(){
                                marker=0;
                            }
                        }.start();
                    }
                }.start();
            }

            public void onFinish() {

                clearAfterStep1();
                initStep2();
            }
        }.start();
    }

    private void initStep2() {
        step = 1;
        posSelected = getAllocNum();
        button.setText(numsGenerated.get(posSelected).toString());
    }

    private void clearAfterStep1() {
        Button a = findViewById(ids[0]);
        Button b = findViewById(ids[1]);
        Button c = findViewById(ids[2]);
        Button d = findViewById(ids[3]);
        Button e = findViewById(ids[4]);
        Button f = findViewById(ids[5]);
        Button g = findViewById(ids[6]);
        Button h = findViewById(ids[7]);
        Button i = findViewById(ids[8]);

        button.setText("");
        a.setText("");
        b.setText("");
        c.setText("");
        d.setText("");
        e.setText("");
        f.setText("");
        g.setText("");
        h.setText("");
        i.setText("");
    }

    private void initUI() {
        setContentView(R.layout.activity_main);
        Button refreshButton = findViewById(R.id.refresh);
        refreshButton.setOnClickListener(this);
        Button connectButton = findViewById(R.id.connect);
        connectButton.setOnClickListener(this);
        Button disconnectButton = findViewById(R.id.disconnect);
        disconnectButton.setOnClickListener(this);

        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        Spinner musesSpinner = findViewById(R.id.muses_spinner);
        musesSpinner.setAdapter(spinnerAdapter);
    }

    private void ensurePermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // We don't have the ACCESS_COARSE_LOCATION permission so create the dialogs asking
            // the user to grant us the permission.

            DialogInterface.OnClickListener buttonListener =
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                    0);
                        }
                    };

            // This is the context dialog which explains to the user the reason we are requesting
            // this permission.  When the user presses the positive (I Understand) button, the
            // standard Android permission dialog will be displayed (as defined in the button
            // listener above).
            AlertDialog introDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.permission_dialog_title)
                    .setMessage(R.string.permission_dialog_description)
                    .setPositiveButton(R.string.permission_dialog_understand, buttonListener)
                    .create();
            introDialog.show();
        }
    }

    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if (btAdapter == null) {
            errorExit("Bluetooth not supported");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth ON...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    private void errorExit(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }

    private int getAllocNum() {
        return (int) (Math.random() * (numsGenerated.size()));
    }

    private int getNum() {
        return (int) (Math.random() * (9 + 1));
    }

    private int genRandom(){
        return (int) (Math.random() * (100 + 1));
    }

    private int getTextPosition() {
        return (int) (Math.random() * (8 + 1));
    }

    public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {

        final ConnectionState current = p.getCurrentConnectionState();

        // Format a message to show the change of connection state in the UI.
        final String status = p.getPreviousConnectionState() + " -> " + current;
        Log.i(TAG, status);

        // Update the UI with the change in connection state.
        handler.post(new Runnable() {
            @Override
            public void run() {

                final TextView statusText = findViewById(R.id.con_status);
                statusText.setText(status);

                final MuseVersion museVersion = muse.getMuseVersion();
                // If we haven't yet connected to the headband, the version information
                // will be null.  You have to connect to the headband before either the
                // MuseVersion or MuseConfiguration information is known.
            }
        });

        if (current == ConnectionState.DISCONNECTED) {
            Log.i(TAG, "Muse disconnected:" + muse.getName());
            // Save the data file once streaming has stopped.
            saveFile();
            // We have disconnected from the headband, so set our cached copy to null.
            this.muse = null;
        }
    }

    public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
        //writeDataPacketToFile(p);

        // valuesSize returns the number of data values contained in the packet.
        final long n = p.valuesSize();
        switch (p.packetType()) {
            case EEG:
                if (BuildConfig.DEBUG && eegBuffer.length < n) throw new AssertionError();
                getEegChannelValues(eegBuffer, p);
                //eegStale = true;
                break;
            default:
                break;
        }
    }

    private void saveFile() {
        Handler h = fileHandler.get();
        if (h != null) {
            h.post(new Runnable() {
                @Override
                public void run() {
                    MuseFileWriter w = fileWriter.get();
                    // Annotation strings can be added to the file to
                    // give context as to what is happening at that point in
                    // time.  An annotation can be an arbitrary string or
                    // may include additional AnnotationData.
                    w.addAnnotationString(0, "Disconnected");
                    w.flush();
                    w.close();
                }
            });
        }
    }

    public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
    }

    private void getEegChannelValues(double[] buffer, MuseDataPacket p) {
        buffer[0] = p.getEegChannelValue(Eeg.EEG1);
        buffer[1] = p.getEegChannelValue(Eeg.EEG2);
        buffer[2] = p.getEegChannelValue(Eeg.EEG3);
        buffer[3] = p.getEegChannelValue(Eeg.EEG4);
        buffer[4] = p.getEegChannelValue(Eeg.AUX_LEFT);
        buffer[5] = p.getEegChannelValue(Eeg.AUX_RIGHT);
        buffer[6] = p.timestamp();
        if (eegStale) {
            dataFile = dataFile + String.valueOf(buffer[6]) + ","
                    + String.format(Locale.US, "%6.2f", buffer[0]) + ","
                    + String.format(Locale.US, "%6.2f", buffer[1]) + ","
                    + String.format(Locale.US, "%6.2f", buffer[2]) + ","
                    + String.format(Locale.US, "%6.2f", buffer[3]) + ","
                    + marker + ",\n";
            //Log.e("Anything",dataFile);
        }

    }

    public void writeToFile(String data)
    {
        String path = Environment.getExternalStorageDirectory() + "/"+folderName+"/";
        // Create the folder.
        File folder = new File(path);
        boolean x=folder.mkdirs();

        // Create the file.
        File file = new File(folder, fileName + ".csv");

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private void writeDataPacketToFile(final MuseDataPacket p) {
        Handler h = fileHandler.get();
        if (h != null) {
            h.post(new Runnable() {
                @Override
                public void run() {
                    fileWriter.get().addDataPacket(0, p);
                }
            });
        }
    }

    public void museListChanged() {
        final List<Muse> list = manager.getMuses();
        spinnerAdapter.clear();
        for (Muse m : list) {
            spinnerAdapter.add(m.getName() + " - " + m.getMacAddress());
        }
    }

    private class MuseL extends MuseListener {
        final WeakReference<MainActivity> activityRef;

        MuseL(final WeakReference<MainActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void museListChanged() {
            activityRef.get().museListChanged();
        }
    }

    private class ConnectionListener extends MuseConnectionListener {
        final WeakReference<MainActivity> activityRef;

        ConnectionListener(final WeakReference<MainActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseConnectionPacket(final MuseConnectionPacket p, final Muse muse) {
            activityRef.get().receiveMuseConnectionPacket(p, muse);
        }
    }

    private class DataListener extends MuseDataListener {
        final WeakReference<MainActivity> activityRef;

        DataListener(final WeakReference<MainActivity> activityRef) {
            this.activityRef = activityRef;
        }

        @Override
        public void receiveMuseDataPacket(final MuseDataPacket p, final Muse muse) {
            activityRef.get().receiveMuseDataPacket(p, muse);
        }

        @Override
        public void receiveMuseArtifactPacket(final MuseArtifactPacket p, final Muse muse) {
            activityRef.get().receiveMuseArtifactPacket(p, muse);
        }
    }
}